#include<stdio.h> 
  
int fact(int n) 
{ 
    if (n <= 1) 
        return 1; 
    return n*fact(n-1); 
} 
  
int nPr(int n, int r) 
{ 
    int perm;
    perm=fact(n)/fact(n-r); 
    printf("%d",perm);
} 
  
int main() 
{ 
    int n, r; 
    printf("Enter n and r: "); 
    scanf("%d %d",&n,&r);
    nPr(n, r); 
    
    return 0; 
}
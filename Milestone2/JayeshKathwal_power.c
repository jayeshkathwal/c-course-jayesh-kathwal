#include<stdio.h> 
double power(double x, unsigned int y) 
{ 
    if (y == 0) 
        return 1; 
    else if (y%2 == 0) 
        return power(x, y/2)*power(x, y/2); 
    else
        return x*power(x, y/2)*power(x, y/2); 
} 
  int main() 
{ 
    double x;
    int y;
    printf("Enter two numbers:");
    scanf("%lf %d",&x,&y);
    printf("%lf", power(x, y)); 
    return 0; 
} 